
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS program;
DROP TABLE IF EXISTS distribution;
DROP TABLE IF EXISTS sport;
DROP TABLE IF EXISTS gym;
DROP TABLE IF EXISTS teacher;

CREATE TABLE IF NOT EXISTS sport (
  sport_ID int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (sport_ID)
);

CREATE TABLE IF NOT EXISTS gym (
  gym_ID int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (gym_ID)
);

CREATE TABLE IF NOT EXISTS teacher (
  teacher_ID int(11) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  PRIMARY KEY (teacher_ID)
);

CREATE TABLE IF NOT EXISTS student (
  student_ID int(11) NOT NULL AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  sport_ID int(11) DEFAULT NULL,
  PRIMARY KEY (`student_ID`),
  KEY sport_ID (`sport_ID`),
  CONSTRAINT student_ibfk_1 FOREIGN KEY (sport_ID) REFERENCES sport (sport_ID)
);


CREATE TABLE IF NOT EXISTS distribution (
  teacher_id int(11) NOT NULL,
  sport_id int(11) NOT NULL,
  PRIMARY KEY (teacher_id,sport_id),
  KEY sport_id (sport_id),
  CONSTRAINT distribution_ibfk_1 FOREIGN KEY (teacher_id) REFERENCES teacher (teacher_ID),
  CONSTRAINT distribution_ibfk_2 FOREIGN KEY (sport_id) REFERENCES sport (sport_ID)
);

CREATE TABLE IF NOT EXISTS program (
  program_id int(11) NOT NULL AUTO_INCREMENT,
  gym_id int(11) NOT NULL,
  sport_id int(11) NOT NULL,
  date_start timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  date_end timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (program_id),
  KEY gym_id (gym_id),
  KEY sport_id (sport_id),
  CONSTRAINT program_ibfk_2 FOREIGN KEY (sport_id) REFERENCES sport (sport_ID)
);



CREATE TABLE IF NOT EXISTS reservation (
  reservation_id int(11) NOT NULL AUTO_INCREMENT,
  program_ID int(11) DEFAULT NULL,
  teacher_ID int(11) DEFAULT NULL,
  PRIMARY KEY (reservation_id),
  KEY program_ID (program_ID),
  KEY teacher_ID (teacher_ID),
  CONSTRAINT reservation_ibfk_2 FOREIGN KEY (teacher_ID) REFERENCES teacher (teacher_ID)
);










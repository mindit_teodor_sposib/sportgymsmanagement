package database.service;

import database.entity.Reservation;
import database.entity.Sport;
import database.entity.Teacher;
import database.repositories.SportRepository;
import database.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    SportRepository sportRepository;
    @Autowired
    ReservationService reservationService;

    @Override
    public ArrayList<Teacher> getAll() {
        ArrayList<Teacher> teachers = (ArrayList<Teacher>) teacherRepository.findAll();
        Collections.sort(teachers,new Comparator<Teacher>() {
            @Override
            public int compare(Teacher fruit2, Teacher fruit1)
            {
                if(fruit2.getLast_name().compareTo(fruit1.getLast_name()) == 0){
                    return  fruit2.getFirst_name().compareTo(fruit1.getFirst_name());
                }
                return  fruit2.getLast_name().compareTo(fruit1.getLast_name());
            }
        });
        return teachers;
    }

    @Override
    public void insertNewTeacher(String first_name, String last_name, int id) {
        Teacher teacher = new Teacher();
        System.out.println(1);
        teacher.setFirst_name(first_name);
        System.out.println(2);
        teacher.setLast_name(last_name);
        ArrayList<Sport> sports = new ArrayList<Sport>();
        sports.add(sportRepository.findOne(id));
        Set<Sport> set = new HashSet<Sport>(sports);
        teacher.setSport(set);
        teacherRepository.saveAndFlush(teacher);

    }



    @Override
    public void updateTeacher(int teacher_id, int sport_id) {
        Teacher teacher = teacherRepository.findOne(teacher_id);
        ArrayList<Sport> sports = teacher.getSport();
        for(int i = 0; i < sports.size(); i++){
            System.out.println(1);
            System.out.println(sports.get(i).getName());
        }
        Sport sport = sportRepository.findOne(sport_id);
        sports.add(sport);
        for(int i = 0; i < sports.size(); i++){
            System.out.println(2);
            System.out.println(sports.get(i).getName());
        }
        Set<Sport> items = new HashSet<Sport>(sports);
        teacher.setSport(items);
        teacherRepository.saveAndFlush(teacher);
    }



    @Override
    public void deleteTeacher(int id) {

        ArrayList<Reservation> reservations = reservationService.getAll();
        for(int i = 0; i < reservations.size(); i++){
            if(reservations.get(i).getTeacher().getTeacher_id() == id){
                reservationService.deleteReservation(reservations.get(i).getReservation_id());
            }
        }
        teacherRepository.delete(teacherRepository.findOne(id));
    }

    @Override
    public void removeSport(int teacher_id, int sport_id) {

            Teacher teacher = teacherRepository.findOne(teacher_id);
            Sport sport = sportRepository.findOne(sport_id);
            ArrayList<Sport> sports = teacher.getSport();
            for(int j = 0; j < sports.size(); j++) {
                if (sports.get(j).getSport_id() == sport_id) {

                    sports.remove(sport);
                    Set<Sport> items = new HashSet<Sport>(sports);
                    teacher.setSport(items);
                    teacherRepository.saveAndFlush(teacher);

                }
            }

    }
}


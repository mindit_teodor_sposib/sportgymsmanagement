package database.service;

import database.entity.Program;
import database.entity.Reservation;
import database.entity.Sport;
import database.entity.Teacher;
import database.repositories.ProgramRepository;
import database.repositories.ReservationRepository;
import database.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by MINDIT-PC on 07/23/2017.
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    ProgramService programService;
    @Autowired
    ProgramRepository programRepository;
    @Autowired
    TeacherRepository teacherRepository;


    @Override
    public ArrayList<Reservation> getAll() {

        ArrayList<Reservation> reservations = (ArrayList<Reservation>) reservationRepository.findAll();
        Collections.sort(reservations,new Comparator<Reservation>() {
            @Override
            public int compare(Reservation fruit2, Reservation fruit1)
            {
                if(fruit2.getProgram().getDay().compareTo(fruit1.getProgram().getDay()) == 0){
                    return  fruit2.getProgram().getDay().compareTo(fruit1.getProgram().getDay());
                }
                return  fruit2.getProgram().getStart().compareTo(fruit1.getProgram().getStart());
            }
        });
        return reservations;
    }

    @Override
    public void insertReservation(int program_id, int teacher_id) {

        Program program = programRepository.findOne(program_id);
        Teacher teacher = teacherRepository.findOne(teacher_id);
        ArrayList<Sport> sports = teacher.getSport();
        for(int i = 0; i < sports.size(); i++){
            if(program.getSport() != null){
                if(sports.get(i).getSport_id() == program.getSport().getSport_id()){
                    Reservation reservation = new Reservation();
                    reservation.setProgram(programRepository.findOne(program_id));
                    reservation.setTeacher(teacher);
                    reservationRepository.saveAndFlush(reservation);
                }
            }

        }


    }

    @Override
    public void deleteReservation(int reservation_id) {
        reservationRepository.delete(reservation_id);
    }

    @Override
    public ArrayList<Program> getProgramsWithoutTeacher() {
        ArrayList<Reservation> reservations= (ArrayList<Reservation>) reservationRepository.findAll();
        ArrayList<Program> reservationWithoutProgram = (ArrayList<Program>) programService.getAll();
        ArrayList<Program> programs = new ArrayList<Program>();

        for(int i = 0; i < reservationWithoutProgram.size(); i++){
            boolean ok = true;
            for(int j = 0; j < reservations.size();j++){
                if(reservationWithoutProgram.get(i).getProgram_id() == reservations.get(j).getProgram().getProgram_id()){
                    ok = false;
                }
            }
            if(ok){
                programs.add(reservationWithoutProgram.get(i));
            }
        }
        Collections.sort(reservationWithoutProgram,new Comparator<Program>() {
            @Override
            public int compare(Program fruit2, Program fruit1)
            {
                if(fruit2.getDay().compareTo(fruit1.getDay()) == 0){
                    return  fruit2.getStart().compareTo(fruit1.getStart());
                }
                return  fruit2.getDay().compareTo(fruit1.getDay());
            }
        });

        return reservationWithoutProgram;
    }
}

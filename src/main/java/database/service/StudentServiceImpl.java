package database.service;

import database.entity.Sport;
import database.entity.Student;
import database.repositories.SportRepository;
import database.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/18/2017.
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepositoryInterface;

    @Autowired
    SportRepository sportRepository;

    @Override
    public ArrayList<Student> getAll() {
        return (ArrayList<Student>) studentRepositoryInterface.findAll();
    }

    @Override
    public void insertNewStudent(String firstName, String lastName, int sport_id) {
        Student student = new Student();
        student.setLast_name(lastName);
        student.setFirst_name(firstName);
        student.setSport(sportRepository.findOne(sport_id));
        studentRepositoryInterface.saveAndFlush(student);

    }

    @Override
    public void updateStudent(int id, int sport_id) {
        Student student = new Student();
        student.setFirst_name(studentRepositoryInterface.findOne(id).getFirst_name());
        student.setLast_name(studentRepositoryInterface.findOne(id).getLast_name());
        student.setSport(sportRepository.findOne(sport_id));
        studentRepositoryInterface.saveAndFlush(student);
    }

    @Override
    public void deleteStudent(int id) {
        studentRepositoryInterface.delete(studentRepositoryInterface.findOne(id));
    }



}

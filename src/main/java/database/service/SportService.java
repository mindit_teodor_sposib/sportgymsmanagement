package database.service;

import database.entity.Sport;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
public interface SportService {

    @Transactional(readOnly = true)
    ArrayList<Sport> getAll();

    public ArrayList<Sport> getSportsWithoutCoach();


    public void insertNewSport(Sport student);
    public void deleteSport(int id);

}

package database.service;

import database.entity.Gym;
import database.entity.Program;
import database.entity.Reservation;
import database.entity.Sport;
import database.repositories.GymRepository;
import database.repositories.ProgramRepository;
import database.repositories.SportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Service
public class ProgramServiceImpl implements ProgramService{

    @Autowired
    ProgramRepository programRepository;
    @Autowired
    GymRepository gymRepository;
    @Autowired
    SportRepository sportRepository;
    @Autowired
    ReservationService reservationService;



    @Override
    public ArrayList<Program> getAll() {

        ArrayList<Program> programs = (ArrayList<Program>)programRepository.findAll();
        Collections.sort(programs,new Comparator<Program>() {
            @Override
            public int compare(Program fruit2, Program fruit1)
            {
                if(fruit2.getDay().compareTo(fruit1.getDay()) == 0){
                    return  fruit2.getStart().compareTo(fruit1.getStart());
                }
                return  fruit2.getDay().compareTo(fruit1.getDay());
            }
        });
        return programs;
    }

    @Override
    public void insertNewProgram(String gym_id, String sport_id,String day, String date_start, String date_end) {

        try {
            Gym gym = gymRepository.findOne(Integer.parseInt(gym_id));
            Sport sport = sportRepository.findOne(Integer.parseInt(sport_id));
            Program program = new Program();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = dateFormat.parse(day + ' ' + date_start + ":00");
            Timestamp startDate = new java.sql.Timestamp(parsedDate.getTime());
            parsedDate = dateFormat.parse(day + ' ' + date_end + ":00" );
            Timestamp endDate = new java.sql.Timestamp(parsedDate.getTime());

            program.setGym(gym);
            program.setSport(sport);
            program.setDate_start(startDate);
            program.setDate_end(endDate);
            programRepository.saveAndFlush(program);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void updateProgram(Program student) {

    }

    @Override
    public void deleteProgram(String program_id) {
        ArrayList<Reservation> reservations = reservationService.getAll();
        for(int i = 0; i < reservations.size(); i++){
            if(reservations.get(i).getProgram().getProgram_id() == Integer.parseInt(program_id)){
                reservationService.deleteReservation(reservations.get(i).getReservation_id());
            }
        }
        programRepository.delete(programRepository.findOne(Integer.parseInt(program_id)));
    }


}

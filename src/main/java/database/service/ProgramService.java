package database.service;

import database.entity.Program;
import database.entity.Sport;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
public interface ProgramService  {

    @Transactional(readOnly = true)
    ArrayList<Program> getAll();

    public void insertNewProgram(String gym_id,String sport_id,String day,String date_start, String date_end);
    public void updateProgram(Program student);
    public void deleteProgram(String program_id);

}

package database.service;

import database.entity.Program;
import database.entity.Reservation;
import database.entity.Sport;

import javax.transaction.Transactional;
import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/23/2017.
 */
public interface ReservationService {


    ArrayList<Reservation> getAll();

    public void insertReservation(int program_id,int teacher_id);
    public void deleteReservation(int reservation_id);
    public ArrayList<Program> getProgramsWithoutTeacher();
}

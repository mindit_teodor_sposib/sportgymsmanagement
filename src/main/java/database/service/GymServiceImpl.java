package database.service;

import database.entity.Gym;
import database.entity.Reservation;
import database.repositories.GymRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Service
public class GymServiceImpl implements GymService {

    @Autowired
    GymRepository gymRepository;
    @Autowired
    ReservationService reservationService;

    @Override
    public ArrayList<Gym> getAll() {

        ArrayList<Gym> gyms = (ArrayList<Gym>) gymRepository.findAll();
        Collections.sort(gyms,new Comparator<Gym>() {
            @Override
            public int compare(Gym fruit2, Gym fruit1)
            {

                return  fruit2.getName().compareTo(fruit1.getName());
            }
        });
        return gyms;
    }

    @Override
    public void insertNewGym(Gym student) {
        gymRepository.saveAndFlush(student);
    }

    @Override
    public void updateGym(String name,int id) {
        Gym gym = gymRepository.findOne(id);
        gym.setName(name);
        gymRepository.save(gym);
    }

    @Override
    public void deleteGym(int id) {

        ArrayList<Reservation> reservations = reservationService.getAll();
        for(int i = 0; i < reservations.size(); i++){
            if(reservations.get(i).getProgram().getGym().getGym_id() == id){
                reservationService.deleteReservation(reservations.get(i).getReservation_id());
            }
        }
        gymRepository.delete(gymRepository.findOne(id));
    }
}

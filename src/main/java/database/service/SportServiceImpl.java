package database.service;

import database.entity.*;
import database.repositories.ProgramRepository;
import database.repositories.SportRepository;
import database.repositories.StudentRepository;
import database.repositories.TeacherRepository;
import org.hibernate.action.internal.CollectionRecreateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Service
public class SportServiceImpl implements SportService {

    @Autowired
    SportRepository sportRepository;
    @Autowired
    StudentService studentService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    ProgramService programService;
    @Autowired
    ReservationService reservationService;


    @Override
    public ArrayList<Sport> getAll() {

        final ArrayList<Sport> sports = (ArrayList<Sport>) sportRepository.findAll();
        Collections.sort(sports,new Comparator<Sport>() {
            @Override
            public int compare(Sport fruit2, Sport fruit1)
            {

                return  fruit2.getName().compareTo(fruit1.getName());
            }
        });
        return sports;
    }

    @Override
    public ArrayList<Sport> getSportsWithoutCoach() {
        ArrayList<Sport> sports= (ArrayList<Sport>) sportRepository.findAll();
        ArrayList<Sport> sportsWithoutCoach = new ArrayList<Sport>();

        for(int i = 0; i < sports.size(); i++){
            if(sports.get(i).getTeachers().size() == 0){
                sportsWithoutCoach.add(sports.get(i));
            }
        }

        Collections.sort(sportsWithoutCoach,new Comparator<Sport>() {
            @Override
            public int compare(Sport fruit2, Sport fruit1)
            {

                return  fruit2.getName().compareTo(fruit1.getName());
            }
        });

        return sportsWithoutCoach;
    }


    @Override
    public void insertNewSport(Sport student) {
        sportRepository.saveAndFlush(student);
    }


    @Override
    public void deleteSport(int id) {

        ArrayList<Reservation> reservations = reservationService.getAll();
        for(int i = 0; i < reservations.size(); i++){
            if(reservations.get(i).getProgram().getSport().getSport_id() == id){
                reservationService.deleteReservation(reservations.get(i).getReservation_id());
            }
        }

        ArrayList<Teacher> teachers = teacherService.getAll();
        for(int i = 0; i < teachers.size(); i++){
            ArrayList<Sport> sports = teachers.get(i).getSport();
            for(int j = 0; j < sports.size(); j++){
                if(sports.get(j).getSport_id() == id ){
                    teacherService.removeSport(teachers.get(i).getTeacher_id(),id);
                    teacherRepository.save(teachers.get(i));

                }
            }

            if(teachers.get(i).getSport().size() == 0){
                teacherService.deleteTeacher(teachers.get(i).getTeacher_id());
            }
        }

        ArrayList<Student> students = studentService.getAll();
        for(int i = 0; i < students.size(); i++){
            if(students.get(i).getSport().getSport_id() == id){
                studentService.deleteStudent(students.get(i).getStudent_id());
            }
        }

        ArrayList<Program> programs = programService.getAll();
        for(int i = 0; i < programs.size(); i++){
            if(programs.get(i).getSport().getSport_id() == id){
                programService.deleteProgram(programs.get(i).getProgram_id() + "");
            }
        }

        sportRepository.delete(sportRepository.findOne(id));

    }
}

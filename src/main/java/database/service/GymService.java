package database.service;

import database.entity.Gym;
import database.entity.Teacher;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
public interface GymService {

    @Transactional(readOnly = true)
    ArrayList<Gym> getAll();

    public void insertNewGym(Gym student);
    public void updateGym(String name,int id);
    public void deleteGym(int id);

}

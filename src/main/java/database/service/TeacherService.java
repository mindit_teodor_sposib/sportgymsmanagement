package database.service;


import database.entity.Teacher;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
public interface TeacherService {

    @Transactional(readOnly = true)
    ArrayList<Teacher> getAll();

    public void insertNewTeacher(String first_name,String last_name,int id);
    public void updateTeacher(int teacher_id,int sport_id);
    public void deleteTeacher(int id);
    public void removeSport(int teacher_id,int sport_id);
}

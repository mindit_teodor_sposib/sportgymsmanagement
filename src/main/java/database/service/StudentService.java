package database.service;

import database.entity.Student;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/18/2017.
 */

public interface StudentService {

    @Transactional(readOnly = true)
    ArrayList<Student> getAll();

    public void insertNewStudent(String firstName, String lastName, int id);
    public void updateStudent(int id,int sport_id);
    public void deleteStudent(int id);



}

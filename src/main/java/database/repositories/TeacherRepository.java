package database.repositories;

import database.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Integer>{
}

package database.repositories;

import database.entity.Gym;
import database.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Repository
public interface GymRepository extends JpaRepository<Gym,Integer> {
}

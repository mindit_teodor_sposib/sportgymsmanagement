package database.repositories;

import database.entity.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Repository
public interface ProgramRepository extends JpaRepository<Program,Integer>{
}

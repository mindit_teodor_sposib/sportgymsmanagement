package database.repositories;

import database.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MINDIT-PC on 07/18/2017.
 */
@Repository
public interface StudentRepository extends JpaRepository<Student,Integer>{

}

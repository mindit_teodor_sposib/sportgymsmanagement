package database.repositories;

import database.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MINDIT-PC on 07/23/2017.
 */
@Repository
public interface ReservationRepository  extends JpaRepository<Reservation,Integer> {
}

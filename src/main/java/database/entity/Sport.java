package database.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by MINDIT-PC on 07/14/2017.
 */
@Table(name = "sport")
@Entity
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sport_ID")
    private int sport_id;
    @Column(name = "name")
    private String name;
    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "sport")
    private Set<Teacher> teachers;


    public Sport() {
    }

    public Sport(String name) {
        this.name = name;
    }

    public Sport(int sport_id, String name) {
        this.sport_id = sport_id;
        this.name = name;
    }



    public ArrayList<Teacher> getTeachers() {


        ArrayList<Teacher> teacher = new ArrayList<Teacher>(teachers);
        return teacher;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

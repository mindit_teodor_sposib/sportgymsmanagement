package database.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by MINDIT-PC on 07/14/2017.
 */
@Entity
@Table(name = "program")
public class Program {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "program_id")
    private int program_id;

    @ManyToOne
    @JoinColumn(name = "sport_ID")
    private Sport sport;

    @ManyToOne
    @JoinColumn(name = "gym_id")
    private Gym gym;



    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date date_start;

    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date date_end;


    public int getProgram_id() {
        return program_id;
    }

    public void setProgram_id(int program_id) {
        this.program_id = program_id;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getDate_end() {
        return date_end;
    }

    public void setDate_end(Date date_end) {
        this.date_end = date_end;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public String getDay(){

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date_start);
    }

    public String getStart(){

        Format formatter = new SimpleDateFormat("hh:mm");
        return formatter.format(date_start);
    }

    public String getEnd(){

        Format formatter = new SimpleDateFormat("hh:mm");
        return formatter.format(date_end);
    }
}

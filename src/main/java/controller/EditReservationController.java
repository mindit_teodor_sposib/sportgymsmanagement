package controller;

import database.entity.Program;
import database.entity.Reservation;
import database.entity.Sport;
import database.entity.Teacher;
import database.service.ReservationService;
import database.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/23/2017.
 */
@Controller
@RequestMapping(value="/editreservation")
public class EditReservationController {

    @Autowired
    ReservationService reservationService;
    @Autowired
    TeacherService teacherService;

    @RequestMapping(value="")
    public String greeting(Model model){

        ArrayList<Reservation> reservations = reservationService.getAll();
        model.addAttribute("list",reservations);
        ArrayList<Program> reservationsWithoutTeacher = reservationService.getProgramsWithoutTeacher();
        model.addAttribute("programslist",reservationsWithoutTeacher);
        ArrayList<Teacher> teachers = teacherService.getAll();
        model.addAttribute("teacherslist",teachers);
        return "editreservation";
    }

    @PostMapping(value="/insert")
    public String greetingInsert(@ModelAttribute("reservation") String program,
                                 @ModelAttribute("teacher") String teaacher){
        System.out.println(program);
        System.out.println(teaacher);
        reservationService.insertReservation(Integer.parseInt(program),Integer.parseInt(teaacher));
        return "redirect:/editreservation/";
    }

    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("reservation") String reservation){
       reservationService.deleteReservation(Integer.parseInt(reservation));
        return "redirect:/editreservation/";
    }
}

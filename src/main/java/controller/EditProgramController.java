package controller;

import database.entity.Gym;
import database.entity.Program;
import database.entity.Sport;
import database.entity.Student;
import database.repositories.GymRepository;
import database.repositories.SportRepository;
import database.service.GymService;
import database.service.ProgramService;
import database.service.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Controller
@RequestMapping(value="/editprogram")
public class EditProgramController {

    @Autowired
    ProgramService programService;
    @Autowired
    GymService gymService;
    @Autowired
    SportService sportService;




    @RequestMapping(value="")
    public String greeting(Model model){
        ArrayList<Program> program = programService.getAll();
        model.addAttribute("list",program);
        ArrayList<Gym> gyms = gymService.getAll();
        model.addAttribute("listgyms",gyms);
        ArrayList<Sport> sports = sportService.getAll();
        model.addAttribute("listsports",sports);

        return "editProgram";
    }



    @PostMapping(value="/add")
    public String greetingInsert(@ModelAttribute("gym") String gym,
                                 @ModelAttribute("sport") String sport,
                                 @ModelAttribute("day") String day,
                                 @ModelAttribute("dateStart") String dateStart,
                                 @ModelAttribute("dateEnd") String dateEnd){
        programService.insertNewProgram(gym,sport,day,dateStart,dateEnd);
        return "redirect:/editprogram/";
    }


    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("program") String program){
        programService.deleteProgram(program);
        return "redirect:/editprogram/";
    }
}

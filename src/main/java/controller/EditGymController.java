package controller;

import database.entity.Gym;
import database.entity.Sport;
import database.service.GymService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Controller
@RequestMapping(value="/editgym")
public class EditGymController {

    @Autowired
    GymService gymService;

    @RequestMapping(value="")
    public String greeting(Model model){
        ArrayList<Gym> gyms = gymService.getAll();
        model.addAttribute("list",gyms);
        return "editGym";
    }

    @PostMapping(value="/add")
    public String greetingInsert(@ModelAttribute Gym gym){
        gymService.insertNewGym(gym);
        return "redirect:/editgym/";
    }

    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("gym") String gym_id, Model model){
        gymService.deleteGym(Integer.parseInt(gym_id));
        return "redirect:/editgym/";
    }


}

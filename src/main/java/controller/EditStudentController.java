package controller;

import database.entity.Sport;
import database.entity.Student;
import database.entity.Teacher;
import database.service.SportService;
import database.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/18/2017.
 */
@Controller
@RequestMapping(value="/editstudent")
public class EditStudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    SportService sportService;


    @RequestMapping(value="")
    public String greeting(Model model){

        ArrayList<Student> students = studentService.getAll();
        model.addAttribute("studentslist",students);
        ArrayList<Sport> sports = sportService.getAll();
        model.addAttribute("listsports",sports);

        return "editStudent";
    }

    @RequestMapping(value="/add",  method = RequestMethod.POST)
    public String greetingInsert(
            @ModelAttribute("first_name") String first_name,
            @ModelAttribute("last_name") String last_name,
            @ModelAttribute("sport") String sport_id,
            Model model){



        studentService.insertNewStudent(first_name,last_name,Integer.parseInt(sport_id));
        return "redirect:/editstudent/";
    }

    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("student") String student_id, Model model){
        studentService.deleteStudent(Integer.parseInt(student_id));
        return "redirect:/editstudent/";
    }

    @PostMapping(value="/update")
    public String greetingUpdate(@ModelAttribute("student") String student_id,
                                 @ModelAttribute("sport") String sport_id){
        studentService.updateStudent(Integer.parseInt(student_id),Integer.parseInt(sport_id));
        return "redirect:/editstudent/";
    }






}

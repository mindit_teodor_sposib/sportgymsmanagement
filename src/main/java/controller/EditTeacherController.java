package controller;

import database.entity.Sport;
import database.entity.Teacher;
import database.service.SportService;
import database.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Controller
@RequestMapping(value="/editteacher")
public class EditTeacherController {
    @Autowired
    TeacherService teacherService;
    @Autowired
    SportService sportService;

    @RequestMapping(value="")
    public String greeting(Model model){
        ArrayList<Teacher> teachers = teacherService.getAll();
        model.addAttribute("list",teachers);
        ArrayList<Sport> sports = sportService.getAll();
        model.addAttribute("sportslist",sports);
        return "editTeacher";
    }

    @PostMapping(value="/add")
    public String greetingInsert(@ModelAttribute("first_name") String first_name,
                                 @ModelAttribute("last_name") String last_name,
                                 @ModelAttribute("sport") String sport){
        teacherService.insertNewTeacher(first_name,last_name,Integer.parseInt(sport));
        return "redirect:/editteacher/";
    }

    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("teacher") String teacher_id,Model model){
        teacherService.deleteTeacher(Integer.parseInt(teacher_id));
        return "redirect:/editteacher/";
    }

    @PostMapping(value="/update")
    public String greetingUpdate(@ModelAttribute("teacher") String teacher_id,
                                 @ModelAttribute("sport") String sport_id){
        teacherService.updateTeacher(Integer.parseInt(teacher_id),Integer.parseInt(sport_id));
        return "redirect:/editteacher/";
    }
}

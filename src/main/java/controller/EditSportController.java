package controller;

import database.entity.Sport;
import database.entity.Student;
import database.entity.Teacher;
import database.service.SportService;
import database.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * Created by MINDIT-PC on 07/19/2017.
 */
@Controller
@RequestMapping(value="/editsport")
public class EditSportController {
    @Autowired
    SportService sportService;

    @Autowired
    TeacherService teacherService;

    @RequestMapping(value="")
    public String greeting(Model model){

        ArrayList<Sport> sports = sportService.getAll();
        ArrayList<Teacher> teachers = teacherService.getAll();
        ArrayList<Sport> sportsWithoutCoach = sportService.getSportsWithoutCoach();
        model.addAttribute("teacherslist",teachers);
        model.addAttribute("list",sports);
        model.addAttribute("sportslist",sportsWithoutCoach);
        return "editSport";
    }

    @PostMapping(value="/add")
    public String greetingInsert(@ModelAttribute Sport sport){

        sportService.insertNewSport(sport);
        return "redirect:/editsport/";
    }

    @PostMapping(value="/delete")
    public String greetingInsert(@ModelAttribute("sport") String sport_id,Model model){
        sportService.deleteSport(Integer.parseInt(sport_id));
        return "redirect:/editsport/";
    }

}
